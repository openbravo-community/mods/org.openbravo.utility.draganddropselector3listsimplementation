/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013-2019 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
// = OBDragAndDropSelectorItem =
// OBDragAndDropSelectorItem is a selector that allows selecting multiple records
// based on a provided list specially intended for be used in parameter windows
isc.ClassFactory.defineClass('OBDragAndDropSelectorItem', isc.CanvasItem);

isc.OBDragAndDropSelectorItem.addProperties(
  isc.addProperties({}, OB.Styles.OBFormField.DefaultComboBox)
);

isc.OBDragAndDropSelectorItem.addProperties({
  colSpan: 2,
  height: '150px',

  init: function() {
    var hLayout;
    // Inside the Selector Item, there is an HLayout
    hLayout = isc.OBDragAndDropSelectorHLayout.create({
      selectorItem: this
    });

    this.canvas = hLayout;
    this.Super('init', arguments);
    this.selectionLayout = this.canvas;
  },

  getValue: function() {
    // Return the selected values in the second grid as the Result
    var value = [],
      list1 = [],
      list2 = [],
      grid2 = this.canvas.grid2,
      i = 0,
      grid3 = this.canvas.grid3;
    for (i = 0; i < grid2.getData().length; i++) {
      list1.add(grid2.getData().get(i).value);
    }
    for (i = 0; i < grid3.getData().length; i++) {
      list2.add(grid3.getData().get(i).value);
    }
    value.add(list1);
    value.add(list2);
    return value;
  },

  destroy: function() {
    // Explicitly destroy the selectotionLayout to avoid memory leaks
    if (this.selectionLayout) {
      this.selectionLayout.destroy();
      this.selectionLayout = null;
    }
    this.Super('destroy', arguments);
  }
});

isc.ClassFactory.defineClass('OBDragAndDropSelectorHLayout', isc.HLayout);
// Inside the HLayout there are two grids, one populated using a DataSource
// and another one that will be used to return the result.
// The values can be moved from one list to the other, and reordered
// using Drag And Drop
isc.OBDragAndDropSelectorHLayout.addProperties({
  membersMargin: 10,
  height: '150px',
  init: function() {
    this.Super('init', arguments);

    this.grid1 = isc.OBGrid.create({
      fields: [
        {
          name: 'name',
          title: OB.I18N.getLabel('DADTLI_DragAndDropDimensions')
        }
      ],
      hLayout: this,
      dragTrackerMode: 'record',
      canDragRecordsOut: true,
      canAcceptDroppedRecords: true,
      canRemoveRecords: false,
      canReorderRecords: true,
      canSort: false,
      canGroupBy: false,
      canAutoFitFields: false,
      preventDuplicates: true,
      dragDataAction: 'move',
      height: '150px',
      onRecordDrop: function(
        dropRecords,
        targetRecord,
        index,
        dropPosition,
        sourceWidget
      ) {
        // Prevent from accepting records from another DragAndDrop reference
        if (
          sourceWidget === this.hLayout.grid1 ||
          sourceWidget === this.hLayout.grid2 ||
          sourceWidget === this.hLayout.grid3
        ) {
          return true;
        } else {
          return false;
        }
      }
    });

    this.grid2 = isc.OBGrid.create({
      fields: [
        {
          name: 'name',
          title: OB.I18N.getLabel('DADTLI_DragAndDropColumn')
        }
      ],
      hLayout: this,
      dragTrackerMode: 'record',
      canDragRecordsOut: true,
      canAcceptDroppedRecords: true,
      canRemoveRecords: false,
      canReorderRecords: true,
      canSort: false,
      canGroupBy: false,
      canAutoFitFields: false,
      preventDuplicates: true,
      dragDataAction: 'move',
      height: '150px',
      emptyMessage:
        '<br /><br />' + OB.I18N.getLabel('DADTLI_DragAndDropEmpty'),
      onRecordDrop: function(
        dropRecords,
        targetRecord,
        index,
        dropPosition,
        sourceWidget
      ) {
        // Prevent from accepting records from another DragAndDrop reference
        if (
          sourceWidget === this.hLayout.grid1 ||
          sourceWidget === this.hLayout.grid2 ||
          sourceWidget === this.hLayout.grid3
        ) {
          return true;
        } else {
          return false;
        }
      }
    });

    this.grid3 = isc.OBGrid.create({
      fields: [
        {
          name: 'name',
          title: OB.I18N.getLabel('DADTLI_DragAndDropRow')
        }
      ],
      hLayout: this,
      dragTrackerMode: 'record',
      canDragRecordsOut: true,
      canAcceptDroppedRecords: true,
      canRemoveRecords: false,
      canReorderRecords: true,
      canSort: false,
      canGroupBy: false,
      canAutoFitFields: false,
      preventDuplicates: true,
      dragDataAction: 'move',
      height: '150px',
      emptyMessage:
        '<br /><br />' + OB.I18N.getLabel('DADTLI_DragAndDropEmpty'),
      onRecordDrop: function(
        dropRecords,
        targetRecord,
        index,
        dropPosition,
        sourceWidget
      ) {
        // Prevent from accepting records from another DragAndDrop reference
        if (
          sourceWidget === this.hLayout.grid1 ||
          sourceWidget === this.hLayout.grid2 ||
          sourceWidget === this.hLayout.grid3
        ) {
          return true;
        } else {
          return false;
        }
      }
    });

    this.addMember(this.grid1);
    this.addMember(this.grid2);
    this.addMember(this.grid3);

    this.dataSource = OB.Datasource.create({
      // fetch data from the DataSource using _adReferenceID as a Parameter
      layout: this,
      createClassName: '',
      cacheAllData: true,
      dataURL:
        OB.Application.contextUrl +
        'org.openbravo.service.datasource/DragAndDropThreeListsDataSource',
      requestProperties: {
        params: {
          _adReferenceID: this.selectorItem._adReferenceListID
            ? this.selectorItem._adReferenceListID
            : null
        }
      }
    });
    this.dataSource.fetchData(null, function(r, data) {
      this.layout.grid1.setData(data);
    });
  },

  destroy: function() {
    // Explicitly destroy the the resultSet from the DataSource to avoid memory leaks
    if (this.dataSource) {
      this.dataSource.cacheResultSet.destroy();
      this.dataSource.cacheResultSet = null;
    }
    this.Super('destroy', arguments);
  }
});
